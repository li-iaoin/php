-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2024-05-13 11:12:00
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yinyue`
--

-- --------------------------------------------------------

--
-- 表的结构 `gequbiao`
--

CREATE TABLE IF NOT EXISTS `gequbiao` (
  `gqname` varchar(100) NOT NULL,
  `gqleibie` varchar(100) NOT NULL,
  `gqshoucang` int(100) NOT NULL,
  `gqusername` varchar(100) NOT NULL,
  `gqzj` varchar(100) NOT NULL,
  `gqlujing` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `geshou`
--

CREATE TABLE IF NOT EXISTS `geshou` (
  `gsuser` varchar(100) NOT NULL,
  `gsuid` int(11) DEFAULT NULL,
  `gssex` varchar(100) NOT NULL,
  `gsage` int(100) NOT NULL,
  `gsimg` varchar(100) NOT NULL,
  `gsqianming` varchar(100) NOT NULL,
  `gsshoucang` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `guanliyuantongjibiao`
--

CREATE TABLE IF NOT EXISTS `guanliyuantongjibiao` (
  `user` varchar(100) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `upwd` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `yonghuxinxibiao`
--

CREATE TABLE IF NOT EXISTS `yonghuxinxibiao` (
  `yhuid` int(11) DEFAULT NULL,
  `yhuser` int(100) NOT NULL,
  `yhpwd` varchar(100) NOT NULL,
  `yhname` varchar(100) NOT NULL,
  `yhtouxiang` varchar(100) NOT NULL,
  `yhqianming` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `zhuangji`
--

CREATE TABLE IF NOT EXISTS `zhuangji` (
  `zjusername` varchar(100) NOT NULL,
  `zjimg` varchar(100) NOT NULL,
  `zjshoucang` int(100) NOT NULL,
  `zjshijian` varchar(100) NOT NULL,
  `zjjieshao` varchar(100) NOT NULL,
  `zjgeshou` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
