# php文件操作

### fopen 

fopen() 函数用于在 PHP 中打开文件。

此函数的第一个参数含有要打开的文件的名称，第二个参数规定了使用哪种模式来打开文件：

```
<html>
<body>
<?php
$file=fopen("welcome.txt","r");
?>
</body>
</html>
```

文件可能通过下列模式来打开：

| 模式 | 描述                                                         |
| :--- | :----------------------------------------------------------- |
| r    | 只读。在文件的开头开始。                                     |
| r+   | 读/写。在文件的开头开始。                                    |
| w    | 只写。打开并清空文件的内容；如果文件不存在，则创建新文件。   |
| w+   | 读/写。打开并清空文件的内容；如果文件不存在，则创建新文件。  |
| a    | 追加。打开并向文件末尾进行写操作，如果文件不存在，则创建新文件。 |
| a+   | 读/追加。通过向文件末尾写内容，来保持文件内容。              |
| x    | 只写。创建新文件。如果文件已存在，则返回 FALSE 和一个错误。  |
| x+   | 读/写。创建新文件。如果文件已存在，则返回 FALSE 和一个错误。 |

**注释：**如果 fopen() 函数无法打开指定文件，则返回 0 (false)。

## 关闭文件

fclose() 函数用于关闭打开的文件：

```
<?php
$file = fopen("test.txt","r");

//执行一些代码

fclose($file);
?>
```

## 检测文件末尾（EOF）

feof() 函数检测是否已到达文件末尾（EOF）。

在循环遍历未知长度的数据时，feof() 函数很有用。

**注释：**在 w 、a 和 x 模式下，您无法读取打开的文件！

```
if (feof($file)) echo "文件结尾";
```

## 逐行读取文件

fgets() 函数用于从文件中逐行读取文件。

**注释：**在调用该函数之后，文件指针会移动到下一行。

### 实例

下面的实例逐行读取文件，直到文件末尾为止：

```
<?php
$file = fopen("welcome.txt", "r") or exit("无法打开文件!");
// 读取文件每一行，直到文件结尾
while(!feof($file))
{
    echo fgets($file). "<br>";
}
fclose($file);
?>
```

## 逐字符读取文件

fgetc() 函数用于从文件中逐字符地读取文件。

**注释：**在调用该函数之后，文件指针会移动到下一个字符。

### 实例

下面的实例逐字符地读取文件，直到文件末尾为止：

```
<?php
$file=fopen("welcome.txt","r") or exit("无法打开文件!");
while (!feof($file))
{
    echo fgetc($file);
}
fclose($file);
?>

```





## 文件夹操作

#### 创建文件夹

在 PHP 中，可以使用 mkdir() 函数创建新的文件夹。该函数接受两个参数，第一个参数是要创建的文件夹路径，第二个参数是用于设置权限的可选参数。

![image-20240428094016651](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094016651.png)

 #### 写入文件

要向文件中写入内容，可以使用 PHP 的 fwrite() 函数。该函数接受三个参数，第一个参数是文件指针，第二个参数是要写入的内容，第三个参数是可选参数，用于设置写入的字节数。

![image-20240428094143622](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094143622.png)

####  读取文件

要读取文件中的内容，可以使用 PHP 的 file_get_contents() 函数。该函数接受一个参数，即要读取的文件路径。

![image-20240428094259697](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094259697.png)

#### 复制文件

使用 PHP 的 copy() 函数可以复制文件。该函数接受两个参数，分别是要复制的源文件和目标文件路径。

![image-20240428094554349](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094554349.png)

#### 删除文件夹

如果需要删除文件夹及其中的所有文件和子文件夹，可以使用 PHP 的 rmdir() 函数。该函数接受一个参数，即要删除的文件夹路径。

![image-20240428094639163](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094639163.png)

#### 删除文件

删除文件可以使用 PHP 的 unlink() 函数。该函数接受一个参数，即要删除的文件路径。

![image-20240428094712811](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094712811.png)

#### 重命名文件

要重命名文件，可以使用 PHP 的 rename() 函数。该函数接受两个参数，第一个参数是要重命名的文件路径，第二个参数是新的文件名。

![image-20240428094754542](./php%E6%96%87%E4%BB%B6%E6%93%8D%E4%BD%9C.assets/image-20240428094754542.png)

#### 浏览文件夹

scandir() 函数返回指定目录中的文件和目录的数组。

## 语法

scandir(*directory,sorting_order,context*);

| 参数            | 描述                                                         |
| :-------------- | :----------------------------------------------------------- |
| *directory*     | 必需。规定要扫描的目录。                                     |
| *sorting_order* | 可选。规定排列顺序。默认是 0，表示按字母升序排列。如果设置为 SCANDIR_SORT_DESCENDING 或者 1，则表示按字母降序排列。如果设置为 SCANDIR_SORT_NONE，则返回未排列的结果。 |
| *context*       | 可选。规定目录句柄的环境。context 是可修改目录流的行为的一套选项。 |

## 技术细节

| 返回值：       | 成功则返回文件和目录的数组。失败则返回 FALSE。如果 directory 不是一个目录，则抛出 E_WARNING 级别的错误。 |
| :------------- | ------------------------------------------------------------ |
| PHP 版本：     | 5.0+                                                         |
| PHP 更新日志： | PHP 5.4：新增 sorting_order 常量。                           |

------

## 文件上传

通过使用 PHP 的全局数组 $_FILES，你可以从客户计算机向远程服务器上传文件。

第一个参数是表单的 input name，第二个下标可以是 "name"、"type"、"size"、"tmp_name" 或 "error"。如下所示：

- $_FILES["file"]["name"] - 上传文件的名称
- $_FILES["file"]["type"] - 上传文件的类型
- $_FILES["file"]["size"] - 上传文件的大小，以字节计
- $_FILES["file"]["tmp_name"] - 存储在服务器的文件的临时副本的名称
- $_FILES["file"]["error"] - 由文件上传导致的错误代码

这是一种非常简单文件上传方式。基于安全方面的考虑，您应当增加有关允许哪些用户上传文件的限制。



## 保存被上传的文件

上面的实例在服务器的 PHP 临时文件夹中创建了一个被上传文件的临时副本。

这个临时的副本文件会在脚本结束时消失。要保存被上传的文件，我们需要把它拷贝到另外的位置：

```
<?php
// 允许上传的图片后缀
    if ($_FILES["file"]["error"] > 0)
    {
        echo "错误：: " . $_FILES["file"]["error"] . "<br>";
    }
    else
    {
        echo "上传文件名: " . $_FILES["file"]["name"] . "<br>";
        echo "文件类型: " . $_FILES["file"]["type"] . "<br>";
        echo "文件大小: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
        echo "文件临时存储的位置: " . $_FILES["file"]["tmp_name"] . "<br>";
        
        // 判断当前目录下的 upload 目录是否存在该文件
        // 如果没有 upload 目录，你需要创建它，upload 目录权限为 777
        if (file_exists("upload/" . $_FILES["file"]["name"]))
        {
            echo $_FILES["file"]["name"] . " 文件已经存在。 ";
        }
        else
        {
            // 如果 upload 目录不存在该文件则将文件上传到 upload 目录下
            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
            echo "文件存储在: " . "upload/" . $_FILES["file"]["name"];
        }
    }
?>

```

*move_uploaded_file 这个函数只能在post请求中可以用*  

## 上传限制

在这个脚本中，我们增加了对文件上传的限制。用户只能上传 .gif、.jpeg、.jpg、.png 文件，文件大小必须小于 200 kB：

```
<?php
// 允许上传的图片后缀
$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);        // 获取文件后缀名
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/x-png")
|| ($_FILES["file"]["type"] == "image/png"))
&& ($_FILES["file"]["size"] < 204800)    // 小于 200 kb
&& in_array($extension, $allowedExts))
{
    if ($_FILES["file"]["error"] > 0)
    {
        echo "错误：: " . $_FILES["file"]["error"] . "<br>";
    }
    else
    {
        echo "上传文件名: " . $_FILES["file"]["name"] . "<br>";
        echo "文件类型: " . $_FILES["file"]["type"] . "<br>";
        echo "文件大小: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
        echo "文件临时存储的位置: " . $_FILES["file"]["tmp_name"];
    }
}
else
{
    echo "非法的文件格式";
}
?>

```

