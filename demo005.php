<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            img{
                height:100px;
                width:100px;
            }
            .mydiv{
                border: 1px solid red;
                border-radius: 50%;
                background-color: rgb(23,56,78);
                margin-top: 1px;
                float:left;
            }
        </style>   
    </head>
    <body>
       <?php
        foreach($_GET as $k=>$v){
            echo "<br/>GET: ".$k." --->".$v;
        }
        echo "<hr/>";
        foreach($_POST as $k=>$v){
            echo "<br/>POST: ".$k." --->".$v;
        }
        //$_REQUEST 是将$_GET 和$_POST中的参数都进行集中存放
        foreach($_REQUEST as $key=>$v2){
             echo "<br/>REQUEST: ".$key." --->".$v2;
        }
        echo "<hr/>";
        $count=isset($_REQUEST["c"])?$_REQUEST["c"]:20;
        $start_height=isset($_REQUEST["h"])?$_REQUEST["h"]:20;
        $index=1;
        while($index<$count){
        $mydivheight=$start_height+$index*5;
        $mydivwidth=$start_height+$index*5;
        $color1=123+$index*7;$color2=213+$index*6;$color3=5+$index*5;
        $color1=$color1%255;$color2=$color2%255; $color3=$color3%255;
        echo "<div class=\"mydiv\" style=\""
        . "height:${mydivheight}px;"
        . "width:${mydivwidth}px;"
        . " background-color: rgb(${color1},${color2},${color3});"
        . "\">";
        echo "</div>";
            $index++;
        }
            ?>
    </body>
</html>
