
<?php
        function getConnect(){
         //mysqli_connect 创建一个mysql数据库的连接，这个连接用于对数据库的操作
        //mysqli_connect 返回值是一个连接对象或者false
        //$hostname  连接的数据库的ip或者域名
        //$username 使用哪个账户连接数据库
        //$password 连接数据库的密码
        //$database 要连接的数据库
            //写一个函数，用于获取mysql数据库连接
            $hostname="127.0.0.1";
            $username="root";
            $password="123456";
            $database="myshop";
            $c=mysqli_connect($hostname, $username, $password, $database);

            if($c){
                return $c;
            }else{
                return false;
            }
        }
        /**
         * 这个函数执行查询操作
         * @param type $sqlString
         * @return type
         */
        function getDataSetBySQL($sqlString){
            $dataset= array();
             $connect= getConnect();
            //设置连接的字符集
            mysqli_set_charset($connect, "utf-8");
            if($connect){
                 $result=mysqli_query($connect, $sqlString);
                //mysqli_fetch_array 获取结果的某一行
               $row= mysqli_fetch_array($result);
               //通过循环来遍历结果集中所有数据
               $index=0;
               while($row){
                  $dataset[$index]=$row;
                $row= mysqli_fetch_array($result);
                $index++;
               }
            }else{
            }
            //mysqli_close 关闭数据库连接
            mysqli_close($connect);
            return $dataset;
        }
           /**
            *  执行更新、修改、删除操作
            * @param type $sqlString
            * @return type 
            */
        function getResultBySQL($sqlString){
            $dataset= array();
             $connect= getConnect();
            //设置连接的字符集
            mysqli_set_charset($connect, "utf-8");
            if($connect){
                 $result=mysqli_query($connect, $sqlString);
                 if($result){
                     mysqli_close($connect);
                     return true;
                 }else{
                     mysqli_close($connect);
                     return false;
                 }
            }else{
            }
            //mysqli_close 关闭数据库连接
            mysqli_close($connect);
            return $dataset;
        }
?>
